# Docker的安装

目前Docker可以在Linux系统、Windows系统、OSX系统上安装，这个文档介绍4种安装方式，安装Docker版本为1.8.2

具备安装Docker的条件：
 - 64位CPU架构的设备
 - Linux系统内核推荐3.10+
 - Linux内核开启namespace和cgroup功能

## Ubuntu系统安装Docker

支持以下版本:
 - Ubuntu Vivid 15.04
 - Ubuntu Trusty 14.04 (LTS)
 - Ubuntu Precise 12.04 (LTS)
 - Ubuntu Saucy 13.10

在Ubuntu系统中安装Docker需要具备的条件：
```
$ uname -r 
3.11.0-15-generic
```
对于Precise 12.04 (LTS)版本，Docker需要3.13版本，如果内核版本不对，参考以下步骤升级内核版本。

打开一个终端输入：

```
$ sudo apt-get update
$ sudo apt-get install linux-image-generic-lts-trusty
$ sudo reboot
```
重启完后，可以[安装Docker](http://docs.docker.com/installation/ubuntulinux/)

安装步骤：

1.打开终端

2.检查curl工具是否已安装,curl安装命令，已安装执行第3步

`$ which curl`
`$ sudo apt-get install curl`

3.安装最新版：`$ curl -sSL https://get.docker.com/ | sh`

## CentOS-7系统安装Docker
支持版本：
 - CentOS 7.X

在CentOS-7中安装Docker需要具备的条件：
```
$ uname -r 
3.10.0-229.el7.x86_64
```
安装步骤:

1.打开终端

2.添加Docker官方得yum源
```
$ cat >/etc/yum.repos.d/docker.repo <<-EOF
[dockerrepo]
name=Docker Repository
baseurl=https://yum.dockerproject.org/repo/main/centos/7
enabled=1
gpgcheck=1
gpgkey=https://yum.dockerproject.org/gpg
EOF
```
3.更新源:`$ sudo yum update`

4.安装最新版本：`$ sudo yum install docker-engine`

5.自动docker服务：`sudo service docker start`

## OSX系统安装Docker

Docker目前支持Mac OS X 10.6及以上版本。现在Docker官方推出Docker Toolbox工具。

安装步骤如下：

1.下载[Docker Toolbox](https://github.com/docker/toolbox/releases/download/v1.8.2/DockerToolbox-1.8.2.pkg)

2.点击安装下载文件

3.根据提示安装

![](http://docs.docker.com/installation/images/mac-welcome-page.png)

4.继续安装

![](http://docs.docker.com/installation/images/mac-page-two.png)

5.输入密码

![](http://docs.docker.com/installation/images/mac-password-prompt.png)

6.安装完成

![](http://docs.docker.com/installation/images/mac-page-finished.png)

7.运行

![](http://docs.docker.com/installation/images/mac-success.png)

## Windows系统安装Docker

在Windows系统上安装Docker，使用boot2docker工具，[下载boot2docker](https://github.com/boot2docker/boot2docker/releases)

下载完成后，点击运行安装，根据提示完成boot2docker的安装，在安装boot2docker的过程中会安装virtualbox虚拟机。

具体安装步骤参考：http://docs.docker.com/installation/windows/